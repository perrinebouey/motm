import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-user-main',
  templateUrl: './user-main.component.html',
  styleUrls: ['./user-main.component.css']
})
export class UserMainComponent implements OnInit {

  idUser: number;
  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    // get l'id du user connecté via l'url
    this.idUser = Number(this.route.snapshot.paramMap.get('id'));
  }

}
