import { Component, OnInit } from '@angular/core';
import {MotmQ} from '../../models/motmQ.model';
import {MotmQService} from '../../services/motm-q.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MotmData} from '../../models/motmData.model';
import {Stats} from '../../models/stats.model';
import {AllData} from '../../models/allData.model';
import {MotmDataService} from '../../services/motm-data.service';
import {AllDataService} from '../../services/all-data.service';

@Component({
  selector: 'app-admin-main',
  templateUrl: './admin-main.component.html',
  styleUrls: ['./admin-main.component.css']
})
export class AdminMainComponent implements OnInit {

    idAdmin: number;
    csvChoice : string;

    allMotmQ: MotmQ[];
    allStats = new Map();
    selectedMotmQ: MotmQ;
    selectedMotmData: MotmData;
    selectedStats: Stats;
    selectedCommentaries: string[];
    selectedRatings: number[] = [];
    selectedResults: boolean;
    monthlyParticipations: number[] = new Array();
    monthlyMeans: number[] = new Array();
    monthlyInterests: number[] = new Array();
    monthlyRatings: any[] = new Array();
    status: any = 'tout';

    constructor(private  motmqService: MotmQService, private motmDataService: MotmDataService,
                private  allDataService: AllDataService, private  route: ActivatedRoute,
                private  router: Router) {
    }

    ngOnInit(): void {
        // get all of the motm questions
        this.motmqService.getMotmQ().subscribe(allMotmQ => {
            this.allMotmQ = allMotmQ
        })

        // get id of Admin that is connected by the url
        this.idAdmin = Number(this.route.snapshot.paramMap.get('idAdmin'));
        this.allDataService.getAllData().subscribe(allData => {
            this.selectedResults = true;
            this.allStats = allData;
            this.monthlyParticipations = [];
            // sorting of data to send in "all data" graphs
            for (const data of Object.keys(this.allStats)) {
                const monthlyData: AllData = this.allStats[data];
                this.monthlyParticipations.push(Number(monthlyData.monthlyParticipation*100));
                this.monthlyMeans.push(Number(monthlyData.monthlyMean));
                this.monthlyInterests.push(Number(monthlyData.monthlyInterest));
                this.monthlyRatings.push(monthlyData.ratesDistribution);
            }

        });

        this.csvChoice = 'user';
    }

    // function that checks if the select of the results has changed
    onChange(motmId) {
        if (motmId !== 'tout') {
            this.router.navigateByUrl('/admin/' + this.idAdmin + '/motm/' + motmId);
        } else {
        }
    }

    // function that checks if the select of the CVS choice has changed
    onChangeCSV(choice){
        switch(choice) {
          case 'user':
            this.csvChoice = 'user';
            break;
          case 'motmQ':
            this.csvChoice = 'motmQ';
            break;
          case 'motmA':
            this.csvChoice = 'motmA';
            break;
          case 'stats':
            this.csvChoice = 'stats';
            break;
        }
    }

    // begin download of a csv
    downloadCsvSubmit(){
        switch(this.csvChoice) {
          case 'user':
            window.location.replace('http://localhost:8080/download/users');
            break;
          case 'motmQ':
            window.location.replace('http://localhost:8080/download/question');
            break;
          case 'motmA':
            window.location.replace('http://localhost:8080/download/answers');
            break;
          case 'stats':
            window.location.replace('http://localhost:8080/download/stats');
            break;
        }
    }
}
