import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {timeout} from 'rxjs/operators';
import {AllData} from '../models/allData.model';

@Injectable({
  providedIn: 'root'
})
export class AllDataService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = environment.url;
  }

  getAllData(): Observable<any> {
    return this.http.get<any>(`${this.url}/stats/getStats`).pipe(timeout(10000));
  }

}
