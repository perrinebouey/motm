import { TestBed } from '@angular/core/testing';

import { MotmQService } from './motm-q.service';

describe('MotmQService', () => {
  let service: MotmQService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MotmQService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
