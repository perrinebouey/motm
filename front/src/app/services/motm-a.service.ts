import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {timeout} from 'rxjs/operators';
import {MotmA} from '../models/motmA.model';

@Injectable({
  providedIn: 'root'
})
export class MotmAService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = environment.url;
  }

  getMotmAByMotmQandUser(questionId: number, userId: number): Observable<MotmA> {
    return this.http
        .get<MotmA>(`${this.url}/answers/user/question/${questionId}/${userId}`)
        .pipe(timeout(10000));
  }

  addMotmA(motmA: MotmA): Observable<MotmA>{
    return this.http
        .post<MotmA>(`${this.url}/answers/createMotmAnswer`, motmA)
        .pipe(timeout(10000));
  }

  getMotmAByUser(userId: number): Observable<MotmA[]> {
    return this.http
        .get<MotmA[]>(`${this.url}/answers/users/${userId}`)
        .pipe(timeout(10000));
  }

}
