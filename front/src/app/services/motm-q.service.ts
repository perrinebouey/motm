import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {MotmQ} from '../models/motmQ.model';
import {map, timeout} from 'rxjs/operators';
import {User} from '../models/user.model';

@Injectable({
    providedIn: 'root'
})
export class MotmQService {

    private url: string;

    constructor(private http: HttpClient) {
        this.url = environment.url;
    }

    addMotmQ(motmQ: MotmQ): Observable<MotmQ> {
        return this.http.post<MotmQ>(`${this.url}/questions/create`, motmQ).pipe(timeout(10000));
    }

    getMotmQ(): Observable<MotmQ[]> {
        return this.http.get<MotmQ[]>(`${this.url}/questions/all`).pipe(timeout(10000));
    }


    getMotmQById(id: number): Observable<MotmQ> {
        return this.http
            .get<MotmQ>(`${this.url}/questions/${id}`)
            .pipe(timeout(10000));
    }

    getMotmQActual(): Observable<MotmQ> {
        return this.http
            .get<MotmQ>(`${this.url}/questions/now`)
            .pipe(timeout(10000));
    }

}
