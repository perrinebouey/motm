import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {MotmQ} from '../models/motmQ.model';
import {timeout} from 'rxjs/operators';
import {MotmData} from '../models/motmData.model';

@Injectable({
  providedIn: 'root'
})
export class MotmDataService {

  private url: string;

  constructor(private http: HttpClient) {
    this.url = environment.url;
  }

  getMotmData(id: number): Observable<MotmData> {
    return this.http.get<MotmData>(`${this.url}/stats/getStats/${id}`).pipe(timeout(10000));
  }




}
