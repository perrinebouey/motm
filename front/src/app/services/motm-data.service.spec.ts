import { TestBed } from '@angular/core/testing';

import { MotmDataService } from './motm-data.service';

describe('MotmDataService', () => {
  let service: MotmDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MotmDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
