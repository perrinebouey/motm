import { TestBed } from '@angular/core/testing';

import { MotmAService } from './motm-a.service';

describe('MotmAService', () => {
  let service: MotmAService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MotmAService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
