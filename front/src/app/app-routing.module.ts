import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserInfosComponent} from './components/user-infos/user-infos.component';
import {AdminMainComponent} from './pages/admin-main/admin-main.component';
import {UserMainComponent} from './pages/user-main/user-main.component';
import {MotmAnswerComponent} from './components/motm-answer/motm-answer.component';
import {NewMotmQComponent} from './components/new-motm-q/new-motm-q.component';
import {AdminDateResultsComponent} from './components/admin-date-results/admin-date-results.component';



const routes: Routes = [
  { path: 'user/:id', component: UserMainComponent },
  { path: 'user/:id/edit-profile', component: UserInfosComponent },
  { path: 'admin/:idAdmin', component: AdminMainComponent },
  { path: 'user/:idUser/motmQ/:idMotmQ', component: MotmAnswerComponent },
  { path: 'admin/:idAdmin/newUser', component: UserInfosComponent },
  { path: 'admin/:idAdmin/edit-profile', component: UserInfosComponent},
  { path: 'admin/:idAdmin/newMotmQ', component: NewMotmQComponent},
  { path: 'admin/:idAdmin/motm/:idMotmQ', component: AdminDateResultsComponent},
  { path: '', redirectTo: '/user/1', pathMatch: 'full' }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

