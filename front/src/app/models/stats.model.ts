import { defaultsDeep } from 'lodash';
export class Stats {
    id: number
    answerNumber : number
    interest: number
    motmMean: number
    participation: number
    motmQId: number

    constructor(stats?: Partial<Stats>) {
        defaultsDeep(this, stats);
    }
}
