import { defaultsDeep } from 'lodash';
import {MotmQ} from './motmQ.model';
export class MotmA {
    id: number;
    answerTimestamp: string;
    commentText: string;
    motmRating: number;
    motmQId: MotmQ;
    userId: number;

    constructor(motmA?: Partial<MotmA>) {
        defaultsDeep(this, motmA);
    }
}
