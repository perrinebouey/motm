import { defaultsDeep } from 'lodash';
export class MotmData {
    comments: string[]
    stats: any
    ratings: any

    constructor(motmData?: Partial<MotmData>) {
        defaultsDeep(this, motmData);
    }
}
