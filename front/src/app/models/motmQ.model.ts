import { defaultsDeep } from 'lodash';
export class MotmQ {
    id: number;
    descText: string;
    monthValidity: Date;
    questionText: string;

    constructor(motmQ?: Partial<MotmQ>) {
        defaultsDeep(this, motmQ);
    }
}
