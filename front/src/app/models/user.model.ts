import { defaultsDeep } from 'lodash';
export class User {
    id: number;
    birthDate: Date;
    firstName: string;
    isAdmin: boolean;
    lastName: string;
    userAvatar: string;
    userMail: string;
    constructor(user?: Partial<User>) {
        defaultsDeep(this, user);
    }
}
