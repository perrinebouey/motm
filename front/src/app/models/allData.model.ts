import { defaultsDeep } from 'lodash';
export class AllData {
    monthlyParticipation: number;
    monthlyMean: number;
    monthlyInterest: number;
    ratesDistribution: number[];

    constructor(allData?: Partial<AllData>) {
        defaultsDeep(this, allData);
    }
}
