import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user.model';

@Component({
  selector: 'app-admin-toolbar',
  templateUrl: './admin-toolbar.component.html',
  styleUrls: ['./admin-toolbar.component.css']
})

export class AdminToolbarComponent implements OnInit {

  admin: User;
  @Input () idAdmin: number;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getUser(this.idAdmin).subscribe(admin => this.admin = admin);
  }

}
