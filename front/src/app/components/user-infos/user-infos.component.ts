import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import { defaultsDeep } from 'lodash';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalDirective} from 'angular-bootstrap-md';
import { AfterViewInit } from '@angular/core';
import {User} from '../../models/user.model';

@Component({
    selector: 'app-edit-user-profile',
    templateUrl: './user-infos.component.html',
    styleUrls: ['./user-infos.component.css']
})
export class UserInfosComponent implements OnInit, AfterViewInit {

    // declaration of the form
    editProfileForm = new FormGroup({
        firstName: new FormControl('', Validators.required),
        lastName: new FormControl('', Validators.required),
        email: new FormControl('', [Validators.required,
            Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
        birthDate: new FormControl('', Validators.required),
        avatar: new FormControl('', Validators.required),
        admin: new FormControl(false, Validators.required)
    });

    user : User;
    mainUser: User;
    idUser : number;
    userPage : boolean; // say if we are in a user page or admin page
    newUser: boolean; // say if we trying to add a new user or not

    @ViewChild('frame', {static: true}) frame: ModalDirective;

    constructor(private userService: UserService, private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        // check the page where we are
        if (this.router.url.startsWith('/user')) {
            this.idUser = Number(this.route.snapshot.paramMap.get('id'));
            this.userPage = true;
            this.newUser = false;
            this.userService.getUser(this.idUser).subscribe(user => this.mainUser = user);
        }else {
            this.idUser = Number(this.route.snapshot.paramMap.get('idAdmin'));
            this.userPage = false;
            this.newUser = false;
            this.userService.getUser(this.idUser).subscribe(user => this.mainUser = user);
        }
        if(this.router.url.endsWith('newUser')){
            this.newUser = true;
        }
    }

    // show the pop-up for edit profile or new user and check if
    // we are in the context of edit-profile. If yes we complete
    // the form with user informations.
    ngAfterViewInit() {
        if(this.newUser === false){
            this.updateProfile();
        }
        this.frame.show();
    }

    // function that send user informations in the form
    updateProfile() {
        this.userService.getUser(this.idUser)
            .subscribe(userResponse => {
                    this.editProfileForm.patchValue({
                        firstName: userResponse.firstName,
                        lastName: userResponse.lastName,
                        email: userResponse.userMail,
                        avatar: userResponse.userAvatar,
                        birthDate: userResponse.birthDate
                    })
                }
            );
    }

    // fonction that is activated when the user click on the button "Valider"
    onFormSubmit() {
        // check we are in the context of adding a new user
        // If yes, we send an id of 0 and the API knows that this is a new user
        // If no, we send the actuel user id
        if(this.newUser === true){
            this.user = this.newAddUser(0, this.editProfileForm.value.admin);
        } else{
            this.user = this.newAddUser(this.idUser, this.mainUser.isAdmin);
        }

        // check if we are in a user page, if yes we redirect to user main page
        // if no, we redirect to admin main page
        if(this.userPage === true){
            this.userService.addUser(this.user).subscribe(() => {
                setTimeout(() => {
                    this.router.navigateByUrl('/user/'+this.idUser)
                }, 300);
            });
        }
        else{
            this.userService.addUser(this.user).subscribe(() => {
                setTimeout(() => {
                    this.router.navigateByUrl('/admin/'+this.idUser)
                }, 300);
            });
        }
    }

    // fonction that creates a new user with informations in the form
    newAddUser(idUpdateUser: number, adminUser: boolean): User{
        const user = defaultsDeep({
            id: idUpdateUser,
            firstName: this.editProfileForm.value.firstName,
            lastName: this.editProfileForm.value.lastName,
            birthDate: this.editProfileForm.value.birthDate,
            isAdmin: adminUser,
            userAvatar: this.editProfileForm.value.avatar,
            userMail: this.editProfileForm.value.email
        });
        return user;
    }

}
