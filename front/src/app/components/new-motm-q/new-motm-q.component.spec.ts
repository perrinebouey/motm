import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewMotmQComponent } from './new-motm-q.component';

describe('NewMotmQComponent', () => {
  let component: NewMotmQComponent;
  let fixture: ComponentFixture<NewMotmQComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewMotmQComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewMotmQComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
