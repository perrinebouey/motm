import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalDirective} from 'angular-bootstrap-md';
import {MotmQService} from '../../services/motm-q.service';
import { defaultsDeep } from 'lodash';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-new-motm-q',
  templateUrl: './new-motm-q.component.html',
  styleUrls: ['./new-motm-q.component.css']
})
export class NewMotmQComponent implements OnInit, AfterViewInit {

  // declare the form
  newMotmQForm = new FormGroup({
    date: new FormControl('', Validators.required),
    question: new FormControl('', Validators.required),
    description: new FormControl(''),
  });

  idAdmin : number;

  @ViewChild('frame', {static: true}) frame: ModalDirective;

  constructor(private motmQService: MotmQService, private route : ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    this.idAdmin = Number(this.route.snapshot.paramMap.get('idAdmin'));
  }

  // show the frame after the wiew is initialize
  ngAfterViewInit() {
    this.frame.show();
  }

  // function that is launched when the user click on the button "Valider"
  onFormSubmit() {
    const motmQ = defaultsDeep({
      id: 0,
      monthValidity: this.newMotmQForm.value.date,
      questionText: this.newMotmQForm.value.question,
      descText: this.newMotmQForm.value.description,
    });
    this.motmQService.addMotmQ(motmQ).subscribe(() => {
      setTimeout(() => {
        this.router.navigateByUrl('/admin/'+this.idAdmin)
      }, 300);
    });
  }

}
