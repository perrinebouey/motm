import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user.model';

@Component({
  selector: 'app-user-toolbar',
  templateUrl: './user-toolbar.component.html',
  styleUrls: ['./user-toolbar.component.css']
})
export class UserToolbarComponent implements OnInit {

  user: User;
  @Input () idUser: number;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getUser(this.idUser).subscribe(user => this.user = user);
  }

}
