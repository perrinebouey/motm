import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MotmAnswerComponent } from './motm-answer.component';

describe('MotmAnswerComponent', () => {
  let component: MotmAnswerComponent;
  let fixture: ComponentFixture<MotmAnswerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MotmAnswerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MotmAnswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
