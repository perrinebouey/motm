import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalDirective} from 'angular-bootstrap-md';
import {ActivatedRoute, Router} from '@angular/router';
import {MotmQService} from '../../services/motm-q.service';
import {MotmQ} from '../../models/motmQ.model';
import { defaultsDeep } from 'lodash';
import {MotmAService} from '../../services/motm-a.service';
import {DatePipe} from '@angular/common';
import {MotmA} from '../../models/motmA.model';

@Component({
  selector: 'app-motm-answer',
  templateUrl: './motm-answer.component.html',
  styleUrls: ['./motm-answer.component.css']
})
export class MotmAnswerComponent implements OnInit, AfterViewInit {

  // declare form
  surveyMotmForm = new FormGroup({
    rating: new FormControl('', Validators.required),
    comment: new FormControl('')
  });

  idUser: number;
  idMotmQ: number;

  motmQ: MotmQ;
  motmA: MotmA;

  @ViewChild('frame', {static: true}) frame: ModalDirective;

  constructor(private route: ActivatedRoute, private router: Router,
              private motmQService: MotmQService,
              private motmAServie: MotmAService,
              private datepipe: DatePipe) { }

  ngOnInit(): void {
    this.idUser = Number(this.route.snapshot.paramMap.get('idUser'));
    this.idMotmQ = Number(this.route.snapshot.paramMap.get('idMotmQ'));
    this.motmQService.getMotmQById(this.idMotmQ).subscribe(motmQ => this.motmQ = motmQ);
    this.motmAServie.getMotmAByMotmQandUser(this.idMotmQ, this.idUser).subscribe(motmA => {
      this.motmA = motmA
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      if(this.motmA !== null) {
        this.updateAnswer();
      }
        this.frame.show();
    }, 300);
  }

  updateAnswer(){
    this.motmAServie.getMotmAByMotmQandUser(this.idMotmQ, this.idUser)
        .subscribe(answerResponse => {
              this.surveyMotmForm.patchValue({
                rating: answerResponse.motmRating.toString(),
                comment: answerResponse.commentText
              })
            }
        );
  }

  onFormSubmit() {
    if(this.motmA !== null){
      this.createMotmA(this.motmA.id);
    }else {
      this.createMotmA(0);
    }
  }

  createMotmA(idMotmA : number){
    const motmA = defaultsDeep({
      id: idMotmA,
      answerTimestamp: this.datepipe.transform(new Date(), 'yyyy-MM-dd'),
      commentText: this.surveyMotmForm.value.comment,
      motmRating: this.surveyMotmForm.value.rating,
      motmQId: this.idMotmQ,
      userId: this.idUser
    });
    this.motmAServie.addMotmA(motmA).subscribe(() => {
      setTimeout(() => {
        this.router.navigateByUrl('/user/'+this.idUser)
      }, 300);
    });
  }

}
