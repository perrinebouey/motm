import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OldSurveysComponent } from './old-surveys.component';

describe('OldSurveysComponent', () => {
  let component: OldSurveysComponent;
  let fixture: ComponentFixture<OldSurveysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OldSurveysComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OldSurveysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
