import {Component, Input, OnInit} from '@angular/core';
import {MotmAService} from '../../services/motm-a.service';
import {MotmA} from '../../models/motmA.model';

@Component({
  selector: 'app-old-surveys',
  templateUrl: './old-surveys.component.html',
  styleUrls: ['./old-surveys.component.css']
})
export class OldSurveysComponent implements OnInit {

  @Input() idUser: number;
  answers: MotmA[];
  constructor(private motmAService: MotmAService) { }

  ngOnInit(): void {
    // get answers of the user that is connected
    this.motmAService.getMotmAByUser(this.idUser).subscribe(motmA => {
      this.answers = motmA
    });
  }

}
