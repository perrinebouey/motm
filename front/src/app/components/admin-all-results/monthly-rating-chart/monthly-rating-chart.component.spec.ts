import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyRatingChartComponent } from './monthly-rating-chart.component';

describe('MonthlyRatingChartComponent', () => {
  let component: MonthlyRatingChartComponent;
  let fixture: ComponentFixture<MonthlyRatingChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonthlyRatingChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyRatingChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
