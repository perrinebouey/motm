import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import * as Highcharts from 'highcharts';
import * as moment from 'moment';

@Component({
  selector: 'app-monthly-rating-chart',
  templateUrl: './monthly-rating-chart.component.html',
  styleUrls: ['./monthly-rating-chart.component.css']
})
export class MonthlyRatingChartComponent implements OnInit, OnChanges {

  @Input() motmFirstRatings;
  @Input() motmSecondRatings;
  @Input() motmThirdRatings;
  firstRatings: number[] = new Array()
  currentDate = moment().format('MM-yyyy');
  oneMonthsAgo = moment().subtract(1, 'months').format('MM-yyyy');
  twoMonthsAgo = moment().subtract(2, 'months').format('MM-yyyy');
  threeMonthsAgo = moment().subtract(3, 'months').format('MM-yyyy');
  fourMonthsAgo = moment().subtract(4, 'months').format('MM-yyyy');
  fiveMonthsAgo = moment().subtract(5, 'months').format('MM-yyyy');

  constructor() { }

  // charts initialization
  highcharts = Highcharts;
  chartOptions = {
    chart: {
      type: 'column',
      style: {
        fontFamily: 'Raleway'
      }
    },
    title: {
      text: 'Répartition des votes'
    },
    xAxis:{
      categories: ['0','1','2','3','4','5'],
      crosshair: true
    },
    yAxis : {
      min: 0,
      title: {
        text: 'Nombre de réponses'
      }
    },
    tooltip : {
      headerFormat: '<span style = "font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style = "color:{series.color};padding:0">{series.name}: </td>' +
          '<td style = "padding:0"><b>{point.y:.0f} votes</b></td></tr>', footerFormat: '</table>', shared: true, useHTML: true
    },
    plotOptions : {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [],
    renderTo: 'voteGraph',
    colors: ['#e34f33','#165160', '#FFC07B' ]
  };

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void{
    // send data to the series components
    // allow to see motm ratings by date
    this.chartOptions.series = [
      {
      name: this.currentDate,
      data: this.motmFirstRatings
      },
      {
        name: this.oneMonthsAgo,
        data: this.motmSecondRatings
      },
      {
        name: this.twoMonthsAgo,
        data: this.motmThirdRatings
      }
    ]
  }

}
