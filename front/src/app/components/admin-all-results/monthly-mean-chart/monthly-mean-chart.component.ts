import {Component, Input, OnChanges, OnInit} from '@angular/core';
import * as moment from 'moment';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-monthly-mean-chart',
  templateUrl: './monthly-mean-chart.component.html',
  styleUrls: ['./monthly-mean-chart.component.css']
})
export class MonthlyMeanChartComponent implements OnInit, OnChanges {

  @Input() allMotmMean;
  currentDate = moment().format('MM-yyyy');
  oneMonthsAgo = moment().subtract(1, 'months').format('MM-yyyy');
  twoMonthsAgo = moment().subtract(2, 'months').format('MM-yyyy');
  threeMonthsAgo = moment().subtract(3, 'months').format('MM-yyyy');

  constructor() { }

  // charts initialization
  highcharts = Highcharts;
  chartOptions = {
    chart: {
      type: 'spline',
      style: {
        fontFamily: 'Raleway'
      }
    },
    title: {
      text: 'Moyenne des derniers mois'
    },
    xAxis:{
      categories: [this.twoMonthsAgo, this.oneMonthsAgo, this.currentDate ],
    },
    yAxis : {
      min: 0,
      max: 5,
      title: {
        text: 'Moyenne des enquêtes'
      }
    },
    tooltip : {
      headerFormat: '<span style = "font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style = "color:{series.color};padding:0">{series.name}: </td>' +
          '<td style = "padding:0"><b>{point.y:.2f} de moyenne</b></td></tr>', footerFormat: '</table>', shared: true, useHTML: true
    },
    plotOptions : {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [],
    renderTo: 'voteGraph',
    colors: ['#E34F33' ]
  };

  ngOnInit() {
  }

  ngOnChanges(): void {
    // send data to the series components
    // allow to see motm mean
    this.chartOptions.series = [{
      name: 'Moyenne',
      data: this.allMotmMean.reverse()
    }]
  }
}
