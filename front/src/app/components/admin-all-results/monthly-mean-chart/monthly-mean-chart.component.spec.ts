import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyMeanChartComponent } from './monthly-mean-chart.component';

describe('MonthlyMeanChartComponent', () => {
  let component: MonthlyMeanChartComponent;
  let fixture: ComponentFixture<MonthlyMeanChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonthlyMeanChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyMeanChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
