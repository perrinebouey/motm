import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyParticipationChartComponent } from './monthly-participation-chart.component';

describe('MonthlyParticipationChartComponent', () => {
  let component: MonthlyParticipationChartComponent;
  let fixture: ComponentFixture<MonthlyParticipationChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonthlyParticipationChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyParticipationChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
