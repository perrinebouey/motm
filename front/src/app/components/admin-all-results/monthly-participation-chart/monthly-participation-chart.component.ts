import {Component, Input, OnChanges, OnInit} from '@angular/core';
import * as Highcharts from 'highcharts';
import {type} from 'os';
import * as moment from 'moment';

@Component({
  selector: 'app-monthly-participation-chart',
  templateUrl: './monthly-participation-chart.component.html',
  styleUrls: ['./monthly-participation-chart.component.css']
})
export class MonthlyParticipationChartComponent implements OnInit, OnChanges {

  @Input() allMotmParticipation
  currentDate = moment().format('MM-yyyy');
  oneMonthsAgo = moment().subtract(1, 'months').format('MM-yyyy');
  twoMonthsAgo = moment().subtract(2, 'months').format('MM-yyyy');


  constructor() { }

  // charts initialization
  highcharts = Highcharts;
  chartOptions = {
    chart: {
      type: 'spline',
      style: {
        fontFamily: 'Raleway'
      }
    },
    title: {
      text: 'Taux de participation des derniers mois',
    },
    xAxis:{
       categories: [this.twoMonthsAgo, this.oneMonthsAgo, this.currentDate ],
    },
    yAxis : {
      min: 0,
      max: 100,
      title: {
        text: '% de participation'
      }
    },
    tooltip : {
      headerFormat: '<span style = "font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style = "color:{series.color};padding:0">{series.name}: </td>' +
      '<td style = "padding:0"><b>{point.y:.0f} % de participation</b></td></tr>', footerFormat: '</table>', shared: true, useHTML: true
    },
    plotOptions : {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [],
    renderTo: 'voteGraph',
    colors: ['#165160' ]
  };

  ngOnInit() {
  }

  ngOnChanges(): void {
    // send data to the series components
    // allow to see motm participation
    this.chartOptions.series = [{
      name: 'Participation',
      data: this.allMotmParticipation.reverse()
    }]
  }

}
