import {Component, Input, OnChanges, OnInit} from '@angular/core';
import * as moment from 'moment';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-monthly-interests-chart',
  templateUrl: './monthly-interests-chart.component.html',
  styleUrls: ['./monthly-interests-chart.component.css']
})
export class MonthlyInterestsChartComponent implements OnInit, OnChanges {

  @Input() allMotmInterest
  currentDate = moment().format('MM-yyyy');
  oneMonthsAgo = moment().subtract(1, 'months').format('MM-yyyy');
  twoMonthsAgo = moment().subtract(2, 'months').format('MM-yyyy');
  threeMonthsAgo = moment().subtract(3, 'months').format('MM-yyyy');

  constructor() { }

  // charts initialization
  highcharts = Highcharts;
  chartOptions = {
    chart: {
      type: 'spline',
      style: {
        fontFamily: 'Raleway'
      }
    },
    title: {
      text: 'Interêt mesuré par mois'
    },
    xAxis:{
      categories: [this.twoMonthsAgo, this.oneMonthsAgo, this.currentDate ],
    },
    yAxis : {
      min: 0,
      max: 5,
      title: {
        text: 'Interêt'
      }
    },
    tooltip : {
      headerFormat: '<span style = "font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style = "color:{series.color};padding:0">{series.name}: </td>' +
          '<td style = "padding:0"><b>{point.y:.2f} d"intérêt</b></td></tr>', footerFormat: '</table>', shared: true, useHTML: true
    },
    plotOptions : {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [],
    renderTo: 'voteGraph',
    colors: ['#FFC07B' ]
  };

  ngOnInit() {
  }

  ngOnChanges(): void {
    // send data to the series components
    // allow to see motm interest
    this.chartOptions.series = [{
      name: 'Interêt',
      data: this.allMotmInterest.reverse()
    }]
  }

}
