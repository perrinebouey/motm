import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyInterestsChartComponent } from './monthly-interests-chart.component';

describe('MonthlyInterestsChartComponent', () => {
  let component: MonthlyInterestsChartComponent;
  let fixture: ComponentFixture<MonthlyInterestsChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonthlyInterestsChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyInterestsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
