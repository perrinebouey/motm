import {Component, Input, OnInit} from '@angular/core';
import {AllData} from '../../models/allData.model';
import {AllDataService} from '../../services/all-data.service';

@Component({
  selector: 'app-admin-all-results',
  templateUrl: './admin-all-results.component.html',
  styleUrls: ['./admin-all-results.component.css']
})
export class AdminAllResultsComponent implements OnInit {
  @Input() allMotmParticipation
  @Input() allMotmMean
  @Input() allMotmInterest
  @Input() allMotmRatings

  motmFirstRatings : number[] = [];
  motmSecondRatings : number[] = [];
  motmThirdRatings : number[] = [];

  constructor() { }

  ngOnInit(): void {
    // get ratings of three last month
    this.motmFirstRatings = this.getMotmRatings(0);
    this.motmSecondRatings = this.getMotmRatings(1);
    this.motmThirdRatings = this.getMotmRatings(2);
  }

  // function that initialize ratings of one motmQ
  private getMotmRatings(idMotm: number): number[] {
    const ratings: any[] = [0, 0, 0, 0, 0, 0];
    for (const data of Object.entries(this.allMotmRatings[idMotm])) {
      switch (data[0]) {
        case '0':
          ratings[0] = data[1];
          break;
        case '1':
          ratings[1] = data[1];
          break;
        case '2':
          ratings[2] = data[1];
          break;
        case '3':
          ratings[3] = data[1];
          break;
        case '4':
          ratings[4] = data[1];
          break;
        case '5':
          ratings[5] = data[1];
          break;
      }
    }
    return ratings
  }
}
