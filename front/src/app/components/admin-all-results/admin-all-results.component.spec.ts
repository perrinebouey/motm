import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAllResultsComponent } from './admin-all-results.component';

describe('AdminAllResultsComponent', () => {
  let component: AdminAllResultsComponent;
  let fixture: ComponentFixture<AdminAllResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAllResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAllResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
