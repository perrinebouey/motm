import {Component, Input, OnInit} from '@angular/core';
import {MotmQService} from '../../services/motm-q.service';
import {MotmQ} from '../../models/motmQ.model';
import {Router} from '@angular/router';
import {MotmAService} from '../../services/motm-a.service';
import {MotmA} from '../../models/motmA.model';

@Component({
  selector: 'app-current-motm',
  templateUrl: './current-motm.component.html',
  styleUrls: ['./current-motm.component.css']
})
export class CurrentMotmComponent implements OnInit {

  @Input() idUser : number
  motmQ: MotmQ;
  motmA: MotmA;

  constructor(private motmQService: MotmQService, private router: Router, private  motmAService: MotmAService) { }

  ngOnInit(): void {
    // get question of the month
    this.motmQService.getMotmQActual().subscribe(motmQ => {
      this.motmQ = motmQ,
          // get answer of the user in the question of the month
      this.motmAService.getMotmAByMotmQandUser(this.motmQ.id, this.idUser).subscribe(motmA => this.motmA = motmA)
    });
  }

  // function that is called when the user click on the button
  // the function redirect to the survey
  redirectToSurvey(){
    this.router.navigateByUrl('/user/'+this.idUser+'/motmQ/'+this.motmQ.id);
  }

}
