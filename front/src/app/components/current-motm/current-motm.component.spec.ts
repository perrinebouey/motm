import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentMotmComponent } from './current-motm.component';

describe('CurrentMotmComponent', () => {
  let component: CurrentMotmComponent;
  let fixture: ComponentFixture<CurrentMotmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurrentMotmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentMotmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
