import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDateResultsComponent } from './admin-date-results.component';

describe('AdminDateResultsComponent', () => {
  let component: AdminDateResultsComponent;
  let fixture: ComponentFixture<AdminDateResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminDateResultsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDateResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
