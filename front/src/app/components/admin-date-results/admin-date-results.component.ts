import {Component, Input, OnInit} from '@angular/core';
import {Stats} from '../../models/stats.model';
import {MotmQService} from '../../services/motm-q.service';
import {MotmDataService} from '../../services/motm-data.service';
import {ActivatedRoute} from '@angular/router';
import {MotmQ} from '../../models/motmQ.model';
import {MotmData} from '../../models/motmData.model';



@Component({
  selector: 'app-admin-date-results',
  templateUrl: './admin-date-results.component.html',
  styleUrls: ['./admin-date-results.component.css']
})
export class AdminDateResultsComponent implements OnInit {

  motmId: number;
  selectedMotmQ: MotmQ;
  selectedMotmData: MotmData;
  selectedStats: Stats;
  selectedCommentaries: string[];
  selectedRatings : number[] = [];
  selectedResults: boolean;
  idAdmin: number;

  constructor(private motmqService: MotmQService, private motmDataService: MotmDataService,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    // ratings initialization
    this.selectedRatings = [];
    const ratings : any[] = [0, 0, 0, 0, 0, 0];

    // get idAdmin and motm question id
    this.idAdmin = Number(this.route.snapshot.paramMap.get('idAdmin'));
    this.motmId = Number(this.route.snapshot.paramMap.get('idMotmQ'));

    // Get motm question by id
    this.motmqService.getMotmQById(this.motmId)
        .subscribe(motmQ => {
          this.selectedMotmQ = motmQ
        });

    // get data of the motm selected
    this.motmDataService.getMotmData(this.motmId)
        .subscribe(motmData => {
          this.selectedMotmData = motmData,
              this.selectedStats = motmData.stats
          this.selectedCommentaries = motmData.comments
          // sorts the ratings to send in data charts
          for (const data of Object.entries(this.selectedMotmData.ratings)) {
            switch (data[0]) {
              case '0':
                ratings[0] = data[1];
                break;
              case '1':
                ratings[1] = data[1];
                break;
              case '2':
                ratings[2] = data[1];
                break;
              case '3':
                ratings[3] = data[1];
                break;
              case '4':
                ratings[4] = data[1];
                break;
              case '5':
                ratings[5] = data[1];
                break;
            }
          }
          this.selectedRatings = ratings;
        });
    this.selectedResults = true;
  }

}
