import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import * as Highcharts from 'highcharts';
import {UpdateChanges} from 'igniteui-angular/migrations/common/UpdateChanges';

@Component({
  selector: 'app-highcharts',
  templateUrl: './highcharts.component.html',
  styleUrls: ['./highcharts.component.css']
})
export class HighchartsComponent implements OnInit, OnChanges {
  @Input() motmRatings;
  constructor() {  }

  // charts initialization
  highcharts = Highcharts;
  chartOptions = {
    chart: {
      type: 'column',
      style: {
        fontFamily: 'Raleway'
      }
    },
    title: {
      text: 'Répartition des votes'
    },
    xAxis:{
      categories: ['0','1','2','3','4','5'],
      crosshair: true
    },
    yAxis : {
      min: 0,
      title: {
        text: 'Nombre de réponses'
      }
    },
    tooltip : {
      headerFormat: '<span style = "font-size:10px">{point.key}</span><table>',
      pointFormat: '<tr><td style = "color:{series.color};padding:0">{series.name}: </td>' +
          '<td style = "padding:0"><b>{point.y:.0f} votes</b></td></tr>',
          footerFormat: '</table>', shared: true, useHTML: true
    },
    plotOptions : {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    series: [],
    renderTo: 'voteGraph',
    colors: ['#e34f33' ]
  };

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void{
    // send data to the series components
    // allow to see motm ratings
    this.chartOptions.series = [{
      name: 'votant',
      data: this.motmRatings
    }]
  }



}
