import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AdminToolbarComponent } from './components/admin-toolbar/admin-toolbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap/modal';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { UserInfosComponent } from './components/user-infos/user-infos.component';
import { HammerModule } from '@angular/platform-browser';
import { IgxCalendarModule } from 'igniteui-angular';
import { UserToolbarComponent } from './components/user-toolbar/user-toolbar.component';
import { AdminMainComponent } from './pages/admin-main/admin-main.component';
import { UserMainComponent } from './pages/user-main/user-main.component';
import { CurrentMotmComponent } from './components/current-motm/current-motm.component';
import { OldSurveysComponent } from './components/old-surveys/old-surveys.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminDateResultsComponent } from './components/admin-date-results/admin-date-results.component';
import { HighchartsChartComponent } from 'highcharts-angular';
import { HighchartsComponent } from './components/admin-date-results/highcharts/highcharts.component';
import { MotmAnswerComponent } from './components/motm-answer/motm-answer.component';
import {DatePipe} from '@angular/common';
import { NewMotmQComponent } from './components/new-motm-q/new-motm-q.component';
import { AdminAllResultsComponent } from './components/admin-all-results/admin-all-results.component';
import { MonthlyParticipationChartComponent } from './components/admin-all-results/monthly-participation-chart/monthly-participation-chart.component';
import { MonthlyMeanChartComponent } from './components/admin-all-results/monthly-mean-chart/monthly-mean-chart.component';
import { MonthlyInterestsChartComponent } from './components/admin-all-results/monthly-interests-chart/monthly-interests-chart.component';
import { MonthlyRatingChartComponent } from './components/admin-all-results/monthly-rating-chart/monthly-rating-chart.component';


@NgModule({
  declarations: [
    AppComponent,
    AdminToolbarComponent,
    UserInfosComponent,
    UserToolbarComponent,
    AdminMainComponent,
    UserMainComponent,
    CurrentMotmComponent,
    OldSurveysComponent,
    AdminDateResultsComponent,
    HighchartsChartComponent,
    HighchartsComponent,
    MotmAnswerComponent,
    NewMotmQComponent,
    AdminAllResultsComponent,
    MonthlyParticipationChartComponent,
    MonthlyMeanChartComponent,
    MonthlyInterestsChartComponent,
    MonthlyRatingChartComponent
  ],

    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        BrowserAnimationsModule,
        ModalModule.forRoot(),
        ReactiveFormsModule,
        MDBBootstrapModule.forRoot(),
        IgxCalendarModule,
        HammerModule,
        NgbModule
    ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }

