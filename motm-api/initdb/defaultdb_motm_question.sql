INSERT INTO defaultdb.motm_question (motm_id, description_text, month_validity, question_text) VALUES (1, 'Qualité de la cantine', '2020-10-01', 'Comment était la qualité de la cantine ce mois-ci ?');
INSERT INTO defaultdb.motm_question (motm_id, description_text, month_validity, question_text) VALUES (2, 'Qualité de l''ascenceur', '2020-09-01', 'Comment trouvez vous les derniers ascenceurs installés ?');
INSERT INTO defaultdb.motm_question (motm_id, description_text, month_validity, question_text) VALUES (3, 'Qualité de votre vie ', '2020-08-01', 'Comment allez vous ?');
INSERT INTO defaultdb.motm_question (motm_id, description_text, month_validity, question_text) VALUES (4, 'Qualité des locaux', '2020-11-01', 'Comment trouvez vous les locaux ?');
INSERT INTO defaultdb.motm_question (motm_id, description_text, month_validity, question_text) VALUES (5, 'Test Description', '2020-12-01', 'Test envoie sur Postman ok ?');
