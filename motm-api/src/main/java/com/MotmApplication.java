package com;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@SpringBootApplication
public class MotmApplication {

    public static void main(String[] args) {
       new SpringApplicationBuilder(MotmApplication.class).run();
    }
}
