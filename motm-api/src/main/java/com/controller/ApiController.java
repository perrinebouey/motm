package com.controller;

import com.config.EmailPropConfig;
import com.config.WriteCsvToResponse;
import com.dao.MotmADAO;
import com.dao.MotmQDAO;
import com.dao.StatsDao;
import com.dao.UserDAO;
import com.model.MOTMAnswer;
import com.model.MOTMQuestion;
import com.model.MonthlyData;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TreeMap;

@RestController
@RequestMapping("")
@CrossOrigin
@EnableScheduling
public class ApiController {

    protected final UserDAO userDao;
    protected final MotmADAO motmADAO;
    protected final MotmQDAO motmQDAO;
    protected final EmailPropConfig emailCfg;
    protected final SpringTemplateEngine thymeleafTemplateEngine;
    protected final StatsDao statsDao;

    public ApiController(UserDAO userDAO,
                         MotmADAO motmADAO,
                         MotmQDAO motmQDAO,
                         EmailPropConfig emailCfg,
                         SpringTemplateEngine thymeleafTemplateEngine,
                         StatsDao statsDao) {
        this.userDao = userDAO;
        this.motmADAO = motmADAO;
        this.motmQDAO = motmQDAO;
        this.emailCfg = emailCfg;
        this.thymeleafTemplateEngine = thymeleafTemplateEngine;
        this.statsDao = statsDao;
    }

    //---------Fonction de traitement suplémentaire-------\\

    // retourne la date + 1 mois selon le calendrier pour l'interval d'existence d'une MOTM
    public static LocalDate addMonth(LocalDate date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(java.sql.Date.valueOf(date));
        cal.add(Calendar.MONTH, 1);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return LocalDate.parse(dateFormat.format(cal.getTime()));
    }

    //Liste de toutes les réponses all time
    public List<MOTMAnswer> getMotmAllAnswersAllTime(){
        Iterable<MOTMAnswer> it = this.motmADAO.findAll();
        List<MOTMAnswer> motmAnswers = new ArrayList<>();
        it.forEach(motmAnswers::add);
        return motmAnswers;
    }

    //liste des réponse pour 1 mois selon l'ID
    public List<MOTMAnswer> getMotmAllAnswersMonthly(Long monthId){ return this.motmADAO.findAllByQuestionId(monthId); }

    //moyennes des notes pour 1 mois
    public Float getMonthlyMean(Long id) {
        return (float) this.motmADAO.findAllRatingsByMonth(id).stream()
                .mapToInt(Integer::intValue)
                .average()
                .orElse(0.0);
    }

    //pourcentage de participation pour 1 mois
    public Float getMonthlyParticipation(Long id){
        List<MOTMAnswer> ans = this.getMotmAllAnswersMonthly(id);
        return (float) ans.size()/this.userDao.count();
    }

    // retourne la répartion des notes pour faire des graphs en colonnes (les boites à moustaches ne sont pas pertinentes)
    public TreeMap<Integer, Integer> getRatingDistribution(Long id){
        List<Integer> listRatings = this.motmADAO.findAllRatingsByMonth(id);
        TreeMap<Integer, Integer> tmap = new TreeMap<>();

        for (Integer t : listRatings) {
            Integer c = tmap.get(t);
            tmap.put(t, (c == null) ? 1 : c + 1);
        }
        return tmap;
    }

    //retourne l'intérêt par mois de chaque question
    public float getMonthlyInterest(Long id){
        List<MOTMAnswer> listAnswers = getMotmAllAnswersMonthly(id);

        List<String> listComments = new ArrayList<>();
        for (MOTMAnswer str : listAnswers)
            listComments.add(str.getCommentText());

        List<Integer> sizeComments = new ArrayList<>();
        for (String str : listComments)
            sizeComments.add(str.length());

        float variance = 1;
        for(MOTMAnswer motmAnswer : listAnswers){
            if (motmAnswer.getMotmRating() <= 1 || motmAnswer.getMotmRating() == 5){
                variance += 2;
            }else if(motmAnswer.getMotmRating() == 2 || motmAnswer.getMotmRating() == 4){
                variance += 1;
            }
        }

        LocalDate reference = listAnswers.get(0).getMotmQId().getMonthValidity();
        List<Integer> intervals = new ArrayList<>();

        for(MOTMAnswer motmAnswer : listAnswers){
            intervals.add(Period.between(reference, motmAnswer.getAnswerTimestamp()).getDays());
        }

        float participation = this.getMonthlyParticipation(id);
        float commentPercentage = (float) (listComments.size() / listAnswers.size());
        float commentMeanSize = (float) sizeComments.stream().mapToInt(Integer::intValue).average().orElse(0.0);
        float averageResponseTime = (float) intervals.stream().mapToInt(Integer::intValue).average().orElse(0.0);

        //Normalisation
        float commentRate;
        float responseTimeRate;
        double varianceRate = 0.0;

        //mesure de l'extrémité des notations
        if (variance != 0){
            varianceRate = Math.log10(variance / 2) / participation;
        }

        //mesure de la liongueur des comentaire (on se base sur la longueure d'un tweet pour la limite haute)
        if(commentMeanSize > 180){
            commentRate = 1.f;
        }else{
            commentRate = commentMeanSize / 180;
        }

        //temps moyen que les réponses ont mis pour être envoyées
        if(averageResponseTime < 10){
            responseTimeRate = 1.f;
        }else{
            responseTimeRate = ( 30 - averageResponseTime)/30 ;
        }

        return (float) (responseTimeRate +
                commentRate +
                varianceRate +
                commentPercentage +
                participation);
    }


    //--------------FONCTIONS DE TÉLÉCHARGEMENT DE DONNÉES------------\\
    @GetMapping("/download/users")
    public void getUsersCsv(HttpServletResponse res) throws IOException {
        res.setContentType("text/csv");
        res.setHeader("Content-Disposition", "attachment; file=users.csv");
        WriteCsvToResponse.writeUsers(res.getWriter(), this.userDao.findAll());
    }

    @GetMapping("/download/answers")
    public void getAnswersCsv(HttpServletResponse res) throws IOException {
        res.setContentType("text/csv");
        res.setHeader("Content-Disposition", "attachment; file=answers.csv");
        WriteCsvToResponse.writeAnswers(res.getWriter(),  this.motmADAO.findAll());
    }

    @GetMapping("/download/question")
    public void getQuestionsCsv(HttpServletResponse res) throws IOException {
        res.setContentType("text/csv");
        res.setHeader("Content-Disposition", "attachment; file=questions.csv");
        WriteCsvToResponse.writeQuestions(res.getWriter(),  this.motmQDAO.findAll());

    }

    @GetMapping("/download/stats")
    public void getStatsCsv(HttpServletResponse res) throws IOException {
        res.setContentType("text/csv");
        res.setHeader("Content-Disposition", "attachment; file=stats.csv");
        WriteCsvToResponse.writeMonthlyStats(res.getWriter(),  this.statsDao.findAll());
    }


    @Scheduled(cron = "0 09 17 * * ?", zone = "Europe/Paris") // tous les jours à 1h du matin
    public void updateMonthlyStats(){
        MonthlyData monthlyData;

        if(this.statsDao.getByMotmQDateTime(LocalDate.now().withDayOfMonth(1)) != null){
            monthlyData = this.statsDao.getByMotmQDateTime(LocalDate.now().withDayOfMonth(1));

            monthlyData.setMotmQId( monthlyData.getMotmQId() );
            monthlyData.setInterest( this.getMonthlyInterest( monthlyData.getMotmQId().getId()) );
            monthlyData.setAnswerNumber( this.motmADAO.findAllByQuestionId(monthlyData.getMotmQId().getId()).size() );
            monthlyData.setMotmMean( this.getMonthlyMean( monthlyData.getMotmQId().getId() ));
            monthlyData.setParticipation( this.getMonthlyParticipation( monthlyData.getMotmQId().getId() ));

        }else {
            monthlyData = new MonthlyData();
            MOTMQuestion motmQuestion = this.motmQDAO.getByMonthValidity( LocalDate.now().withDayOfMonth(1) );

            monthlyData.setMotmQId( motmQuestion );
            monthlyData.setInterest( getMonthlyInterest( motmQuestion.getId()) );
            monthlyData.setAnswerNumber( this.motmADAO.findAllByQuestionId( motmQuestion.getId()).size() );
            monthlyData.setMotmMean( getMonthlyMean( motmQuestion.getId()) );
            monthlyData.setParticipation( motmQuestion.getId() );
        }

        this.statsDao.save(monthlyData);
    }
}
