package com.controller;

import com.config.EmailPropConfig;
import com.dao.MotmADAO;
import com.dao.MotmQDAO;
import com.dao.StatsDao;
import com.dao.UserDAO;
import com.model.AnswerFromJson;
import com.model.MOTMAnswer;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/answers")
@CrossOrigin
public class AnswersController extends ApiController{

    public AnswersController(UserDAO userDAO, MotmADAO motmADAO, MotmQDAO motmQDAO, EmailPropConfig emailCfg, SpringTemplateEngine thymeleafTemplateEngine, StatsDao statsDao) {
        super(userDAO, motmADAO, motmQDAO, emailCfg, thymeleafTemplateEngine, statsDao);
    }

    // update si id question et id user existe déjà dans la meme entrée
    @PostMapping("/createMotmAnswer")
    MOTMAnswer addMotmAnswer(@RequestBody AnswerFromJson motmAnswer){
        //supprime le commentaire si il fait moins de 3 caractères
        if (motmAnswer.getCommentText().length() < 3){
            motmAnswer.setCommentText("");
        }
        MOTMAnswer res = new MOTMAnswer();
        res.setId(motmAnswer.getId());
        res.setUserId(this.userDao.getOne(motmAnswer.getUserId()));
        res.setMotmQId(this.motmQDAO.getOne(motmAnswer.getMotmQId()));
        res.setCommentText(motmAnswer.getCommentText());
        res.setAnswerTimestamp(LocalDate.now());
        res.setMotmRating(motmAnswer.getMotmRating());
        this.motmADAO.save(res);

        this.updateMonthlyStats();

        return res;
    }

    @GetMapping("/all")
    List<MOTMAnswer> getAllAnswers() { return this.getMotmAllAnswersAllTime(); }

    @GetMapping("/users/{id}")
    List<MOTMAnswer> getUsersMotmAnswers(@PathVariable Long id){
        return this.motmADAO.findAllByUserId(id);
    }

    @GetMapping("/user/question/{questionID}/{userID}")
    MOTMAnswer checkIfUserAnwserExist(@PathVariable Long questionID, @PathVariable Long userID){
        return  this.motmADAO.findUserAnswerForSpecificQuestion(questionID, userID);
    }
}
