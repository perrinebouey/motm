package com.controller;

import com.config.EmailPropConfig;
import com.dao.MotmADAO;
import com.dao.MotmQDAO;
import com.dao.StatsDao;
import com.dao.UserDAO;
import com.model.MOTMQuestion;
import com.model.User;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoField;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

@RestController
@RequestMapping("/mails")
@CrossOrigin
@EnableScheduling
public class MailController extends ApiController{

    public MailController(UserDAO userDAO, MotmADAO motmADAO, MotmQDAO motmQDAO, EmailPropConfig emailCfg, SpringTemplateEngine thymeleafTemplateEngine, StatsDao statsDao) {
        super(userDAO, motmADAO, motmQDAO, emailCfg, thymeleafTemplateEngine, statsDao);
    }

    @Scheduled(cron = "0 0 10 * * ?")
    public void sendToEveryUsers() throws MessagingException {
        //get this month question id
        String[] monthName = {"Janvier", "Fevrier", "Mars",
                "Avril", "Mai", "Juin", "Juillet",
                "Aout", "Septembre", "Octobre",
                "Novembre", "Décembre"};
        Calendar cal = Calendar.getInstance();
        String monthCurrent = monthName[cal.get(Calendar.MONTH)];
        cal.set(Calendar.DAY_OF_MONTH, 1);
        MOTMQuestion motmQuestion = this.motmQDAO.getByMonthValidity(LocalDate.now(ZoneId.of("Europe/Paris")).with(ChronoField.DAY_OF_MONTH, 1));

        // Create template
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(emailCfg.getHost());
        mailSender.setPort(emailCfg.getPort());
        mailSender.setUsername(emailCfg.getUsername());
        mailSender.setPassword(emailCfg.getPassword());

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        List<User> list = this.userDao.findAll();
        //iterate through user to send mail with id in the mail template
        for (User user : list){
            sendSimpleEmail(mailSender, user, motmQuestion , monthCurrent);
        }
    }

    public void sendSimpleEmail(JavaMailSenderImpl mailSender , User user,
                                MOTMQuestion motmQuestion,String month) throws MessagingException {

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, "UTF-8");
        messageHelper.setFrom(emailCfg.getUsername());
        messageHelper.setTo(Objects.requireNonNull(user.getUserMail()));

        Context thymeleafContext = new Context();
        thymeleafContext.setVariable("userId", user.getId());
        thymeleafContext.setVariable("question", "La question du mois est : " + motmQuestion.getQuestionText());
        thymeleafContext.setVariable("month", "Mood of " + month);
        String htmlBody = thymeleafTemplateEngine.process("emailTemplate", thymeleafContext);

        messageHelper.setSubject("MOTM du mois de " + month +" !");
        messageHelper.setText(htmlBody, true);

        mailSender.send(message);
    }
}
