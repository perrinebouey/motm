package com.controller;

import com.config.EmailPropConfig;
import com.dao.MotmADAO;
import com.dao.MotmQDAO;
import com.dao.StatsDao;
import com.dao.UserDAO;
import com.model.MOTMQuestion;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/questions")
@CrossOrigin
public class QuestionsController extends ApiController{

    public QuestionsController(UserDAO userDAO, MotmADAO motmADAO, MotmQDAO motmQDAO, EmailPropConfig emailCfg, SpringTemplateEngine thymeleafTemplateEngine, StatsDao statsDao) {
        super(userDAO, motmADAO, motmQDAO, emailCfg, thymeleafTemplateEngine, statsDao);
    }

    @PostMapping("/create")
    MOTMQuestion addMotmQuestion(@RequestBody MOTMQuestion motmQuestion){

        if (this.motmQDAO.checkIfMomtAlreadyExist(motmQuestion.getMonthValidity(), addMonth(motmQuestion.getMonthValidity())) == 1) {
            return null;
        }
        // faire en sorte que ça update la question ou que ça renvoie une message de confirmation
        return this.motmQDAO.save(motmQuestion);
    }

    @GetMapping("/all")
    List<MOTMQuestion> getAllQuestions() {
        Iterable<MOTMQuestion> it = this.motmQDAO.findAll();
        List<MOTMQuestion> motmQuestions = new ArrayList<>();
        it.forEach(motmQuestions::add);

        return motmQuestions;
    }

    @GetMapping("/{id}")
    MOTMQuestion getMotmQuestionById(@PathVariable Long id){
        return this.motmQDAO.getOne(id);
    }

    @GetMapping("/now")
    MOTMQuestion getMotmQuestionByMonth(){
        return this.motmQDAO.getByMonthValidity(LocalDate.now().withDayOfMonth(1));
    }
}