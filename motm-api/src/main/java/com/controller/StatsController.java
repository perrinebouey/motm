package com.controller;

import com.config.EmailPropConfig;
import com.dao.MotmADAO;
import com.dao.MotmQDAO;
import com.dao.StatsDao;
import com.dao.UserDAO;
import com.model.AllTimeData;
import com.model.MOTMQuestion;
import com.model.MonthlyData;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/stats")
@CrossOrigin
public class StatsController extends ApiController{
    public StatsController(UserDAO userDAO, MotmADAO motmADAO, MotmQDAO motmQDAO, EmailPropConfig emailCfg, SpringTemplateEngine thymeleafTemplateEngine, StatsDao statsDao) {
        super(userDAO, motmADAO, motmQDAO, emailCfg, thymeleafTemplateEngine, statsDao);
    }

    //retourne les stats pour une mois identifié
    @GetMapping("/getStats/{id}")
    @ResponseBody
    HashMap<String, Object> monthlyDataPerQuestionId(@PathVariable Long id){
        MonthlyData data = this.statsDao.getByMotmQId(id);
        HashMap<String, Object> res = new HashMap<>();
        res.put("stats", data);
        res.put("ratings", getRatingDistribution(id));
        res.put("comments", this.motmADAO.findAllCommentsByMonth(id));

        return res;
    }

    //return the general stats data for every month recorded
    @GetMapping("/getStats")
    @ResponseBody
    public HashMap<Long, AllTimeData> monthlyDataAllTime(){

        List<MOTMQuestion> motmQuestions = this.motmQDAO.findAllInPastAndPresent();
        HashMap<Long, AllTimeData> allTimeStats = new HashMap<>();

        for(MOTMQuestion motmQuestion : motmQuestions){
            if (motmQuestion.getMonthValidity().compareTo( addMonth(LocalDate.now().withDayOfMonth(1)) ) < 0){

            AllTimeData allTimeData = new AllTimeData();

            allTimeData.setMonthlyParticipation( getMonthlyParticipation(motmQuestion.getId()));
            allTimeData.setMonthlyMean(getMonthlyMean(motmQuestion.getId()));
            allTimeData.setMonthlyInterest( getMonthlyInterest(motmQuestion.getId()));
            allTimeData.setRatesDistribution(getRatingDistribution(motmQuestion.getId()));

            allTimeStats.put(motmQuestion.getId(), allTimeData);
            }
        }

        return allTimeStats;
    }

    //update les stats du mois tous les jours à 23h55
    @Scheduled(cron = "0 55 23 * * ?", zone = "Europe/Paris") // tous les jours à 1h du matin
    public void updateMonthlyStats(){
        MonthlyData monthlyData;

        if(this.statsDao.getByMotmQDateTime(LocalDate.now().withDayOfMonth(1)) != null){
            monthlyData = this.statsDao.getByMotmQDateTime(LocalDate.now().withDayOfMonth(1));

            monthlyData.setMotmQId( monthlyData.getMotmQId() );
            monthlyData.setInterest( this.getMonthlyInterest( monthlyData.getMotmQId().getId()) );
            monthlyData.setAnswerNumber( this.motmADAO.findAllByQuestionId(monthlyData.getMotmQId().getId()).size() );
            monthlyData.setMotmMean( this.getMonthlyMean( monthlyData.getMotmQId().getId() ));
            monthlyData.setParticipation( this.getMonthlyParticipation( monthlyData.getMotmQId().getId() ));

        }else {
            monthlyData = new MonthlyData();
            MOTMQuestion motmQuestion = this.motmQDAO.getByMonthValidity( LocalDate.now().withDayOfMonth(1) );

            monthlyData.setMotmQId( motmQuestion );
            monthlyData.setInterest( getMonthlyInterest( motmQuestion.getId()) );
            monthlyData.setAnswerNumber( this.motmADAO.findAllByQuestionId( motmQuestion.getId()).size() );
            monthlyData.setMotmMean( getMonthlyMean( motmQuestion.getId()) );
            monthlyData.setParticipation( motmQuestion.getId() );
        }

        this.statsDao.save(monthlyData);
    }

/*    Fonction pour load toutes les monthly stats en fonction de la DB une fois relancée

    @ResponseBody
    public String updateMonthlyStats(){

        List<MOTMQuestion> listData = this.motmQDAO.findAll();
        for(MOTMQuestion motmQuestion: listData){
            MonthlyData monthlyData = new MonthlyData();
            monthlyData.setInterest( this.getMonthlyInterest( motmQuestion.getId()) );
            monthlyData.setAnswerNumber( this.motmADAO.findAllByQuestionId(motmQuestion.getId()).size() );
            monthlyData.setMotmMean( this.getMonthlyMean( motmQuestion.getId() ));
            monthlyData.setParticipation( this.getMonthlyParticipation( motmQuestion.getId() ));
            monthlyData.setMotmQId( motmQuestion );
            this.statsDao.save(monthlyData);
        }
        return "succes";
    }

*/
}
