package com.controller;

import com.EmployeeNotFoundException;
import com.config.EmailPropConfig;
import com.dao.MotmADAO;
import com.dao.MotmQDAO;
import com.dao.StatsDao;
import com.dao.UserDAO;
import com.model.User;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.List;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController extends ApiController {

    public UserController(UserDAO userDAO, MotmADAO motmADAO, MotmQDAO motmQDAO, EmailPropConfig emailCfg, SpringTemplateEngine thymeleafTemplateEngine, StatsDao statsDao) {
        super(userDAO, motmADAO, motmQDAO, emailCfg, thymeleafTemplateEngine, statsDao);
    }

    @GetMapping("/all")
    public List<User> getUsers() {
        return this.userDao.findAll();
    }

    @GetMapping("/{id}")
    User one(@PathVariable Long id) {
        return userDao.findById(id)
                .orElseThrow(() -> new EmployeeNotFoundException(id));
    }

    //permet de modifier si on renseigne un id déjà existant
    @PostMapping("/add")
    public User addUser(@RequestBody User user) {
        return this.userDao.save(user);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUser(@PathVariable Long id) {
        this.userDao.deleteById(id);
    }

}
