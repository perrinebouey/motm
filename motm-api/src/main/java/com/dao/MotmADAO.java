package com.dao;

import com.model.MOTMAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MotmADAO extends JpaRepository<MOTMAnswer, Long> {

    @Query(value = "SELECT * from motm_answers where user_id = ?1", nativeQuery = true)
    List<MOTMAnswer> findAllByUserId(Long id);

    @Query(value = "SELECT * from motm_answers where question_id = ?1", nativeQuery = true)
    List<MOTMAnswer> findAllByQuestionId(Long id);

    @Query(value = "SELECT motm_rating from motm_answers where question_id = ?1", nativeQuery = true)
    List<Integer> findAllRatingsByMonth(Long id);

    @Query(value = "SELECT comment_text from motm_answers where question_id = ?1", nativeQuery = true)
    List<String> findAllCommentsByMonth(Long id);

    @Query(value = "SELECT * from motm_answers where question_id = ?1 && user_id =?2", nativeQuery = true)
    MOTMAnswer findUserAnswerForSpecificQuestion(Long question_id, Long user_id);


}
