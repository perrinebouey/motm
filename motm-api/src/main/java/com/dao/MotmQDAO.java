package com.dao;

import com.model.MOTMQuestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface MotmQDAO extends JpaRepository<MOTMQuestion, Long> {

    //check if motm already exist
    @Query(value ="SELECT COUNT(1) FROM motm_question WHERE  (month_validity >= ?1 and month_validity < ?2 )", nativeQuery = true)
    int checkIfMomtAlreadyExist(LocalDate dateStart, LocalDate dateEnd);

    @Query(value ="SELECT * FROM motm_question WHERE (month_validity = ?1)",nativeQuery = true)
    MOTMQuestion getByMonthValidity(LocalDate date);

    @Query(value = "SELECT * FROM motm_question WHERE month_validity <= NOW()", nativeQuery = true)
    List<MOTMQuestion> findAllInPastAndPresent();
}
