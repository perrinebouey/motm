package com.dao;

import com.model.MonthlyData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;

public interface StatsDao extends JpaRepository<MonthlyData, Long> {

    @Query(value ="SELECT data_id, monthly_answers, monthly_interest, monthly_mean, monthly_participation, question_id FROM monthly_data LEFT JOIN motm_question ON monthly_data.question_id = motm_question.motm_id WHERE month_validity = ?1",nativeQuery = true)
    MonthlyData getByMotmQDateTime(LocalDate currentMonth);

    @Query(value ="SELECT * FROM monthly_data WHERE question_id = ?1",nativeQuery = true)
    MonthlyData getByMotmQId(Long id);
}
