package com.config;


import com.model.MOTMAnswer;
import com.model.MOTMQuestion;
import com.model.MonthlyData;
import com.model.User;
import java.io.PrintWriter;
import java.util.List;

public class WriteCsvToResponse {

    public static void writeUsers(PrintWriter writer, List<User> users) {
        writer.write("user_id, first_name, last_name, user_mail, birth_date,user_avatar, is_admin\n");
        for (User user : users) {
            writer.write(user.getId() + ","
                    + user.getFirstName() + ","
                    + user.getLastName() + ","
                    + user.getUserMail() + ","
                    + user.getBirthDate() + ","
                    + user.getUserAvatar() + ","
                    + user.isAdmin() + "\n");
        }
    }

    public static void writeAnswers(PrintWriter writer, List<MOTMAnswer> motmAnswers) {
        writer.write("answer_id, question_id, user_id, answer_timestamp, motm_rating,comment_text\n");
        for (MOTMAnswer motmAnswer : motmAnswers) {
            writer.write(motmAnswer.getId() + ","
                    + motmAnswer.getMotmQId() + ","
                    + motmAnswer.getUserId() + ","
                    + motmAnswer.getAnswerTimestamp() + ","
                    + motmAnswer.getMotmRating() + ","
                    + motmAnswer.getCommentText() + "\n");
        }
    }

    public static void writeMonthlyStats(PrintWriter writer, List<MonthlyData> monthlyDatas) {
        writer.write("data_id, question_id, monthly_answers, monthly_participation, monthly_mean, monthly_interest\n");

        for (MonthlyData monthlyData : monthlyDatas) {
            writer.write(monthlyData.getId() + ","
                    + monthlyData.getMotmQId() + ","
                    + monthlyData.getAnswerNumber() + ","
                    + monthlyData.getParticipation() + ","
                    + monthlyData.getMotmMean() + ","
                    + monthlyData.getInterest() + "\n");
        }
    }

    public static void writeQuestions(PrintWriter writer, List<MOTMQuestion> motmQuestions) {
        writer.write("motm_id, month_validity, user_id, question_text, description_text\n");
        for (MOTMQuestion motmQuestion : motmQuestions) {
            writer.write(motmQuestion.getId() + ","
                    + motmQuestion.getMonthValidity() + ","
                    + motmQuestion.getQuestionText() + ","
                    + motmQuestion.getDescText() +"\n");
        }
    }
}
