package com.config

import com.model.MOTMAnswer
import com.model.MOTMQuestion
import com.model.MonthlyData
import com.model.User
import java.io.PrintWriter

object WriteCsvToResponse {
    @JvmStatic
    fun writeUsers(writer: PrintWriter, users: List<User>) {
        writer.write("user_id, first_name, last_name, user_mail, birth_date,user_avatar, is_admin\n")
        for ((id, firstName, lastName, userMail, birthDate, userAvatar, isAdmin) in users) {
            writer.write("""
    ${id.toString()},$firstName,$lastName,$userMail,$birthDate,$userAvatar,$isAdmin
    
    """.trimIndent())
        }
    }

    @JvmStatic
    fun writeAnswers(writer: PrintWriter, motmAnswers: List<MOTMAnswer>) {
        writer.write("answer_id, question_id, user_id, answer_timestamp, motm_rating,comment_text\n")
        for ((id, motmQId, userId, answerTimestamp, motmRating, commentText) in motmAnswers) {
            writer.write("""
    ${id.toString()},$motmQId,$userId,$answerTimestamp,$motmRating,$commentText
    
    """.trimIndent())
        }
    }

    @JvmStatic
    fun writeMonthlyStats(writer: PrintWriter, monthlyDatas: List<MonthlyData>) {
        writer.write("data_id, question_id, monthly_answers, monthly_participation, monthly_mean, monthly_interest\n")
        for (monthlyData in monthlyDatas) {
            writer.write("""
    ${monthlyData.id.toString()},${monthlyData.motmQId},${monthlyData.answerNumber},${monthlyData.participation},${monthlyData.motmMean},${monthlyData.interest}
    
    """.trimIndent())
        }
    }

    @JvmStatic
    fun writeQuestions(writer: PrintWriter, motmQuestions: List<MOTMQuestion>) {
        writer.write("motm_id, month_validity, user_id, question_text, description_text\n")
        for ((id, monthValidity, questionText, descText) in motmQuestions) {
            writer.write("""
    ${id.toString()},$monthValidity,$questionText,$descText
    
    """.trimIndent())
        }
    }
}