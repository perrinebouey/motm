package com.model

import javax.persistence.*

@Entity(name = "monthly_data")
class MonthlyData(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "data_id")
        @Id var id: Long = 0L,

        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "question_id", referencedColumnName = "motm_id") var motmQId: MOTMQuestion? = null,
        @Column(name = "monthly_answers") var answerNumber: Int = 0,
        @Column(name = "monthly_participation") var participation: Float = 0F,
        @Column(name = "monthly_mean") var motmMean: Float = 0F,
        @Column(name = "monthly_interest") var interest: Float = 0F) {
}
