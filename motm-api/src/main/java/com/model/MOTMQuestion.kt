package com.model

import java.time.LocalDate
import javax.persistence.*

@Entity(name = "motm_question")
data class MOTMQuestion (
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "motm_id")
        @Id var id: Long?,

        @Column(name = "month_validity") var monthValidity: LocalDate?,
        @Column(name = "question_text") var questionText: String?,
        @Column(name = "description_text") var descText: String?) {
    constructor() : this(null, null, null, null)

}