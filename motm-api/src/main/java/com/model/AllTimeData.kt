package com.model

import java.util.*

class AllTimeData(var monthlyParticipation: Float?,
                  var monthlyMean: Float? ,
                  var monthlyInterest: Float? ,
                  var ratesDistribution: TreeMap<Int, Int>?) {
    constructor() : this(null, null, null, null )

}