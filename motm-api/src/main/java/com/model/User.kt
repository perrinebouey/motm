package com.model

import javax.persistence.*

/**
 *
 */
@Entity(name = "motm_users")
data class User(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "user_id")
        @Id var id: Long?,
        @Column(name = "first_name") var firstName: String?,
        @Column(name = "last_name") var lastName: String?,
        @Column(name = "user_mail") var userMail: String?,
        @Column(name = "birth_date") var birthDate: java.sql.Date?,
        @Column(name = "user_avatar") var userAvatar: String?,
        @Column(name = "is_admin") var isAdmin: Boolean?) {
    constructor() : this(null, null, null, null, null, null,false)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as User

        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false

        return true
    }

}