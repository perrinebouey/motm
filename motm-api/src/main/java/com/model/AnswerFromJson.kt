package com.model

class AnswerFromJson {
    var id: Long = 0
    var motmQId: Long = 0
    var userId: Long = 0
    var motmRating = 0
    var commentText: String? = null
}