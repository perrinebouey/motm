package com.model

import java.time.LocalDate
import javax.persistence.*

@Entity(name = "motm_answers")
data class MOTMAnswer (
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "answer_id")
        @Id var id: Long?,

        @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
        @JoinColumn(name = "question_id", referencedColumnName = "motm_id") var motmQId: MOTMQuestion?,

        @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
        @JoinColumn(name = "user_id", referencedColumnName = "user_id") var userId: User?,

        @Column(name = "answer_timestamp") var answerTimestamp: LocalDate?,
        @Column(name = "motm_rating") var motmRating: Int?,
        @Column(name = "comment_text") var commentText: String?){
    constructor() : this(null,null, null, null,null, null)
}