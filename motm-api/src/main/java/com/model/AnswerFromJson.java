package com.model;

import java.time.LocalDate;

public class AnswerFromJson {
    private long id;
    private long motmQId;
    private long userId;
    private LocalDate answerTimestamp;
    private int motmRating;
    private String commentText;

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }



    public long getMotmQId() {
        return motmQId;
    }

    public void setMotmQId(long motmQId) {
        this.motmQId = motmQId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public LocalDate getAnswerTimestamp() {
        return answerTimestamp;
    }

    public void setAnswerTimestamp(LocalDate answerTimestamp) {
        this.answerTimestamp = answerTimestamp;
    }

    public int getMotmRating() {
        return motmRating;
    }

    public void setMotmRating(int motmRating) {
        this.motmRating = motmRating;
    }
}
