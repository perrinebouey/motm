# MOTM

Pour une meilleure expérience veuillez ouvrir le back et le front dans deux fenêtres d'intellij différentes

## BACK-END

Comment configurer et run le back-end:

* Définir la base de données sur MariaDB, avec pour host :
    - sous Mac :  localhost
    - sous Pc : IP FOURNIE PAR DOCKER

sur le port 3306, avec les identifiants/mdp : root/toor, et comme database : defaultdb

* Exécuter votre DB mysql. Si vous avez docker, vous pouvez utiliser la commande suivante:
```
docker run --name mariadb --rm -e MYSQL_ROOT_PASSWORD=toor -e MYSQL_DATABASE=defaultdb -p 3306:3306 -v "`pwd`/initdb:/docker-entrypoint-initdb.d" mariadb
```
* Sur Mac, vérifier que la configuration de /motm-api/src/main/ressources/application.properties comporte les informations suivantes:
```
spring.datasource.url=jdbc:mariadb://localhost:3306/defaultdb
spring.datasource.username=root
spring.datasource.password=toor
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver
```
* Sur PC, il faut récupérer l'adresse ip retourné par le conteneur Docker et remplacer la *datasource.url* par cette ip:
```
spring.datasource.url=jdbc:mariadb://<IP FOURNIE PAR DOCKER>:3306/defaultdb
spring.datasource.username=root
spring.datasource.password=toor
spring.datasource.driver-class-name=org.mariadb.jdbc.Driver
```
* Configurer le Run avec sur un Spring Boot application, avec pour Main class  *com.MotmApplication*

* Mettre à jour les dépendances maven

* Mettre en place la configuration pour exécuter le back-end : 
    - Dans "Add Configuration", ajouter une nouvelle configuration type Spring Boot
    - Définir la main class sur *com.MotmApplication*
    - Le class path of module doit être sur *motm-api*
    - Vérifier que le JRE est configuré sur le SDK 11 

Run le projet et vous aurez accès à l'API avec l'addresse *http://localhost:8080/*

* Exécuter les scripts SQL situé dans le dossier suivant pour peupler la base de données :
```
motm-api/initdb/defaultdb_motm_answers.sql
motm-api/initdb/defaultdb_motm_question.sql
motm-api/initdb/defaultdb_motm_user.sql
motm-api/initdb/defaultdb_motm_stats.sql
```

## FRONT-END

### Comment configurer et run le front-end:

Après avoir réussis les étapes précédentes et lancer le back end, entrer en ligne de commande dans le dossier front (cd motm/front)

Installer toutes les dépendances nécessaires grâce à NodeJs à l'aide de la commande "npm install"

Configurer le Run sur Angular-CLI-Serveur

Run le projet et vous aurez accès au site internet à l'adresse http://localhost:4200/

Pour voir l'arborescence du front, voir l'image arborescence.png